package app.ceva.petapp.feature.AllProducts;

import javax.inject.Inject;

import app.ceva.petapp.data.DataManager;
import app.ceva.petapp.data.network.model.AllProducts.AllProductRequest;
import app.ceva.petapp.data.network.model.AllProducts.AllProductResponse;
import app.ceva.petapp.share.base.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllProductPresenter  <V extends AllProductMvpView> extends BasePresenter<V> implements AllProductMvpPresenter<V> {

    @Inject
    public AllProductPresenter(DataManager mdataManager) {
        super(mdataManager);
    }

    @Override
    public void onGetAllProdudcts() {
        if(getMvpView().isNetworkConnected())
        {
            getMvpView().showLoading();
            AllProductRequest allProductRequest=new AllProductRequest(getDataManager().getUserId());
            getDataManager().getAllProducts(allProductRequest).enqueue(new Callback<AllProductResponse>() {
                @Override
                public void onResponse(Call<AllProductResponse> call, Response<AllProductResponse> response) {

                    if(response.isSuccessful())
                    {
                        getMvpView().hideLoading();
                        if(response.body().getResponseCode()==1)
                        {
                           // getMvpView().onError(response.body().getResponseText());
                            getMvpView().successfullyGetAllProducts(response.body());

                        }
                        else
                        {
                            getMvpView().onError(response.body().getResponseText());
                            getMvpView().successfullyGetAllProducts(response.body());
                        }
                    }
                    else
                    {
                        getMvpView().hideLoading();
                        getMvpView().onError("Server Error");
                    }

                }

                @Override
                public void onFailure(Call<AllProductResponse> call, Throwable t) {
                    getMvpView().hideLoading();
                    getMvpView().showAlert("Server Error");
                }
            });

        }
        else
        {
            getMvpView().showAlert("No internet Connection");
        }
    }

}
