package app.ceva.petapp.feature.Notification;

import app.ceva.petapp.data.network.model.Notification.NotificationResponse;
import app.ceva.petapp.data.network.model.readnotification.ReadNotificationResponse;
import app.ceva.petapp.share.base.MvpView;

public interface NotificationMvpView extends MvpView {

    void successfullyNotification(NotificationResponse notificationResponse);
    void successfullyReadNotification(ReadNotificationResponse readNotificationResponse);
}
