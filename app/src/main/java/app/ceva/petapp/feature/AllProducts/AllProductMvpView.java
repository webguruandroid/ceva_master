package app.ceva.petapp.feature.AllProducts;

import app.ceva.petapp.data.network.model.AllProducts.AllProductResponse;
import app.ceva.petapp.share.base.MvpView;

public interface AllProductMvpView extends MvpView {

    void successfullyGetAllProducts(AllProductResponse allProductResponse);
}
