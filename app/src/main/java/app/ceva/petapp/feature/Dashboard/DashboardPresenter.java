package app.ceva.petapp.feature.Dashboard;

import javax.inject.Inject;

import app.ceva.petapp.data.DataManager;
import app.ceva.petapp.data.network.model.NotificationCount.NotificationCountResponse;
import app.ceva.petapp.data.network.model.NotificationCount.NotificationCountRqeuest;
import app.ceva.petapp.data.network.model.ScanQr.ScanQrRequest;
import app.ceva.petapp.data.network.model.ScanQr.ScanQrResponse;
import app.ceva.petapp.data.network.model.Slug.SlugResponse;
import app.ceva.petapp.data.network.model.UpdateToken.UpdateTokenRequest;
import app.ceva.petapp.data.network.model.UpdateToken.UpdateTokenResponse;
import app.ceva.petapp.share.base.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardPresenter<V extends DashboardMvpView> extends BasePresenter<V>implements DashboardMvpPresenter<V> {

    @Inject
    public DashboardPresenter(DataManager mdataManager) {
        super(mdataManager);
    }

    @Override
    public void onLogoutClick() {

        getDataManager().logout();
        getMvpView().navigateToLogout();


        if(getMvpView().isNetworkConnected())
        {
            getMvpView().showLoading();
          /*  LogoutRequest updateTokenRequest=new LogoutRequest(getDataManager().getUserId(),deviceType,deviceToken);
            getDataManager().getUpdateToken(updateTokenRequest).enqueue(new Callback<UpdateTokenResponse>() {
                @Override
                public void onResponse(Call<UpdateTokenResponse> call, Response<UpdateTokenResponse> response) {

                    if(response.isSuccessful())
                    {
                        getMvpView().hideLoading();
                        if(response.body().getResponseCode()==1)
                        {

                            getMvpView().ongetUpdateToken(response.body());

                        }
                        else
                        {
                            getMvpView().ongetUpdateToken(response.body());

                        }
                    }
                    else
                    {
                        getMvpView().hideLoading();
                        getMvpView().onError("Server Error");
                    }

                }

                @Override
                public void onFailure(Call<UpdateTokenResponse> call, Throwable t) {
                    getMvpView().hideLoading();
                    getMvpView().showAlert("Server Error");
                }
            });
*/
        }
        else
        {
            getMvpView().showAlert("No internet Connection");
        }


    }

    @Override
    public void onScanQr(String regionId, String productId,String productUniqueId) {

        if(getMvpView().isNetworkConnected())
        {
            getMvpView().showLoading();
            ScanQrRequest scanQrRequest=new ScanQrRequest(getDataManager().getUserId(),regionId,productId,productUniqueId);
            getDataManager().getScanQr(scanQrRequest).enqueue(new Callback<ScanQrResponse>() {
                @Override
                public void onResponse(Call<ScanQrResponse> call, Response<ScanQrResponse> response) {

                    if(response.isSuccessful())
                    {
                        getMvpView().hideLoading();
                        if(response.body().getResponseCode()==1)
                        {
                           // getMvpView().onError(response.body().getResponseText());
                            getMvpView().onSuccessfullScanQr(response.body());

                        }
                        else
                        {
                            getMvpView().onSuccessfullScanQr(response.body());
                           // getMvpView().onError(response.body().getResponseText());
                        }
                    }
                    else
                    {
                        getMvpView().hideLoading();
                        getMvpView().onError("Server Error");
                    }

                }

                @Override
                public void onFailure(Call<ScanQrResponse> call, Throwable t) {
                    getMvpView().hideLoading();
                    getMvpView().showAlert("Server Error");
                }
            });

        }
        else
        {
            getMvpView().showAlert("No internet Connection");
        }


    }

    @Override
    public void onNotificationCount() {
        if(getMvpView().isNetworkConnected())
        {
            getMvpView().showLoading();
            NotificationCountRqeuest notificationCountRqeuest=new NotificationCountRqeuest(getDataManager().getUserId(),"A");
            getDataManager().getNotificationCount(notificationCountRqeuest).enqueue(new Callback<NotificationCountResponse>() {
                @Override
                public void onResponse(Call<NotificationCountResponse> call, Response<NotificationCountResponse> response) {

                    if(response.isSuccessful())
                    {
                        getMvpView().hideLoading();
                        if(response.body().getResponseCode()==1)
                        {

                            getMvpView().ongetNotificationCount(response.body());

                        }
                        else
                        {
                            getMvpView().ongetNotificationCount(response.body());

                        }
                    }
                    else
                    {
                        getMvpView().hideLoading();
                        getMvpView().onError("Server Error");
                    }

                }

                @Override
                public void onFailure(Call<NotificationCountResponse> call, Throwable t) {
                    getMvpView().hideLoading();
                    getMvpView().showAlert("Server Error");
                }
            });

        }
        else
        {
            getMvpView().showAlert("No internet Connection");
        }

    }

    @Override
    public void onUpdateToken(String userId, String deviceType, String deviceToken) {

        if(getMvpView().isNetworkConnected())
        {
            getMvpView().showLoading();
            UpdateTokenRequest updateTokenRequest=new UpdateTokenRequest(getDataManager().getUserId(),deviceType,deviceToken);
            getDataManager().getUpdateToken(updateTokenRequest).enqueue(new Callback<UpdateTokenResponse>() {
                @Override
                public void onResponse(Call<UpdateTokenResponse> call, Response<UpdateTokenResponse> response) {

                    if(response.isSuccessful())
                    {
                        getMvpView().hideLoading();
                        if(response.body().getResponseCode()==1)
                        {

                            getMvpView().ongetUpdateToken(response.body());

                        }
                        else
                        {
                            getMvpView().ongetUpdateToken(response.body());

                        }
                    }
                    else
                    {
                        getMvpView().hideLoading();
                        getMvpView().onError("Server Error");
                    }

                }

                @Override
                public void onFailure(Call<UpdateTokenResponse> call, Throwable t) {
                    getMvpView().hideLoading();
                    getMvpView().showAlert("Server Error");
                }
            });

        }
        else
        {
            getMvpView().showAlert("No internet Connection");
        }


    }

    @Override
    public void onSlugList() {
        //  getMvpView().showLoading();
        if (getMvpView().isNetworkConnected()) {
            getDataManager().getSlugList().enqueue(new Callback<SlugResponse>() {
                @Override
                public void onResponse(Call<SlugResponse> call, Response<SlugResponse> response) {

                    if (response.isSuccessful()) {
                        getMvpView().hideLoading();
                        if (response.body().getResponseCode() == 1) {

                            getMvpView().onGetSlugData(response.body());

                        } else {
                            getMvpView().onGetSlugData(response.body());

                        }
                    } else {
                        getMvpView().hideLoading();
                       // getMvpView().onError("Server Error");
                    }

                }

                @Override
                public void onFailure(Call<SlugResponse> call, Throwable t) {
                    getMvpView().hideLoading();
                   // getMvpView().showAlert("Server Error");
                }
            });

        } else {
            getMvpView().showAlert("No internet Connection");
        }
    }

}
