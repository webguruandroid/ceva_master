package app.ceva.petapp.feature.ProductDetailsFromAllProduct;

import app.ceva.petapp.data.network.model.GetPet.GetPetsResponse;
import app.ceva.petapp.data.network.model.ProductDetailsFromAll.ProductDetailsFromAllResponse;
import app.ceva.petapp.data.network.model.SubmitScheduling.SubmitSchedulingResponse;
import app.ceva.petapp.share.base.MvpView;

public interface ProductDetailsFromAllProductMvpView extends MvpView {
    void successfullyGetProductDetails(ProductDetailsFromAllResponse productDetailsFromAllResponse);
    void successfullyPetList(GetPetsResponse response);
    void successfullyGetSchedule(SubmitSchedulingResponse submitSchedulingResponse);

}


