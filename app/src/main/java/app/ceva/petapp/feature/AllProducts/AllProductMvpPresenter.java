package app.ceva.petapp.feature.AllProducts;

import app.ceva.petapp.share.base.MvpPresenter;

public interface AllProductMvpPresenter <V extends AllProductMvpView> extends MvpPresenter<V> {

    void onGetAllProdudcts();
}
