package app.ceva.petapp.feature.Notification;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.ceva.petapp.R;
import app.ceva.petapp.data.network.model.Notification.NotificationResponse;
import app.ceva.petapp.share.wigeds.TextViewRegular;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder> {


    private final ArrayList<NotificationResponse.ResponseDataBean> mValues;


    private NotificationListListner listListner;


     Context ctx;

    public NotificationListAdapter(ArrayList<NotificationResponse.ResponseDataBean> mValues) {
        this.mValues = mValues;
    }



    public void loadList(List<NotificationResponse.ResponseDataBean> items) {
        mValues.clear();
        mValues.addAll(items);
        notifyDataSetChanged();
    }
    public interface NotificationListListner {
        void onItemClick(NotificationResponse.ResponseDataBean item, int position, String petId, String productId,String UniqueId);
    }


    public void setAdapterListner(NotificationListListner listner)
    {
        this.listListner=listner;
    }


    @NonNull
    @Override
    public NotificationListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ctx = parent.getContext();
        View view = LayoutInflater.from( ctx)
                .inflate(R.layout.layout_notification, parent, false);
        return new NotificationListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        NotificationResponse.ResponseDataBean mDataBean = mValues.get(position);

        holder.tvAnimalName.setText(mDataBean.getPet_name());
        holder.tvProductName.setText(mDataBean.getProduct_name());
        holder.tvDescription.setText(mDataBean.getMessage());
        holder.tvDate.setText(mDataBean.getVaccine_date());
        holder.tvPetId.setText(mDataBean.getPet_id());
        holder.tvProductId.setText(mDataBean.getProduct_id());
        holder.tvProductUniqueId.setText(mDataBean.getProduct_uniqueId());


        if(mDataBean.getIs_read().equals("N"))
        {
            holder.llMain.setBackgroundColor(ctx.getResources().getColor(R.color.colorDeamWhite));
        }
        else
        {
            holder.llMain.setBackgroundColor(ctx.getResources().getColor(R.color.colorWhite));
        }

    }


    @Override
    public int getItemCount() {
        return mValues.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.llMain)
        LinearLayout llMain;
        @BindView(R.id.tvAnimalName)
        TextViewRegular tvAnimalName;
        @BindView(R.id.tvProductName)
        TextViewRegular tvProductName;
        @BindView(R.id.tvDescription)
        TextViewRegular tvDescription;
        @BindView(R.id.tvDate)
        TextViewRegular tvDate;
        @BindView(R.id.tvPetId)
        TextViewRegular tvPetId;
        @BindView(R.id.tvProductId)
        TextViewRegular tvProductId;
        @BindView(R.id.tvProductUniqueId)
        TextViewRegular tvProductUniqueId;





        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);



            llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listListner.onItemClick(mValues.get(getLayoutPosition()),getLayoutPosition(),tvPetId.getText().toString().trim(),tvProductId.getText().toString().trim(),tvProductUniqueId.getText().toString().trim());
                }
            });
        }

    }
}
