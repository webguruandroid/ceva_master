package app.ceva.petapp.feature.AllProducts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.ceva.petapp.R;
import app.ceva.petapp.data.network.model.AllProducts.AllProductResponse;
import app.ceva.petapp.share.wigeds.TextViewRegular;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AllProductAdapter extends RecyclerView.Adapter<AllProductAdapter.ViewHolder> {


    private final ArrayList<AllProductResponse.AllProductBean> mValues;


    private AllProductAdapter.AllProductListListner listListner;


     Context ctx;

    public AllProductAdapter(ArrayList<AllProductResponse.AllProductBean> mValues) {
        this.mValues = mValues;
    }



    public void loadList(List<AllProductResponse.AllProductBean> items) {
        mValues.clear();
        mValues.addAll(items);
        notifyDataSetChanged();
    }
    public interface AllProductListListner{
        void onItemClick(AllProductResponse.AllProductBean item, int position,  String ProductId);
    }



    public void setAdapterListner(AllProductAdapter.AllProductListListner listner)
    {
        this.listListner=listner;
    }



    @NonNull
    @Override
    public AllProductAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ctx = parent.getContext();
        View view = LayoutInflater.from( ctx)
                .inflate(R.layout.row_all_product, parent, false);
        return new AllProductAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        AllProductResponse.AllProductBean mDataBean = mValues.get(position);

        holder.tvProductName.setText(mDataBean.getProduct_name());
        holder.tvProductId.setText(mDataBean.getProduct_id());



    }


    @Override
    public int getItemCount() {
        return mValues.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {




        @BindView(R.id.tvProductName)
        TextViewRegular tvProductName;
        @BindView(R.id.tvProductId)
        TextViewRegular tvProductId;

        @BindView(R.id.llMain)
        LinearLayout llMain;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);



            llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listListner.onItemClick(mValues.get(getLayoutPosition()),getLayoutPosition(),tvProductId.getText().toString().trim());
                }
            });
        }

    }
}
