package app.ceva.petapp.feature.AllProducts;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import javax.inject.Inject;

import app.ceva.petapp.R;
import app.ceva.petapp.data.network.model.AllProducts.AllProductResponse;
import app.ceva.petapp.feature.ProductDetailsFromAllProduct.ProductDetailsFromAllProductActivity;
import app.ceva.petapp.share.base.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AllProductsActivity extends BaseActivity implements AllProductMvpView {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.rv_allProduct)
    RecyclerView rv_allProduct;

    @Inject
    AllProductAdapter adapter;

    @Inject
    AllProductPresenter<AllProductMvpView> presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_products);
        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        setUp();
        setToolbar();
        setRecylerView();
    }

    protected void setUp()
    {
        presenter.onAttached(this);


    }

    private void setToolbar()
    {

        setSupportActionBar(toolbar);
        //  toolbar.setNavigationIcon(R.drawable.ic_left_arrow);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        // upArrow.setColorFilter(getResources().getColor(R.color.grey), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here


                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void setRecylerView() {
        rv_allProduct.setLayoutManager(new LinearLayoutManager(this));
        rv_allProduct.setAdapter(adapter);
        presenter.onGetAllProdudcts();
        adapter.setAdapterListner(new AllProductAdapter.AllProductListListner() {
            @Override
            public void onItemClick(AllProductResponse.AllProductBean item, int position, String ProductId) {
//                Intent ie = new Intent(getActivity(), ProductDetailsActivity.class);
//                ie.putExtra("productId",ProductId );
//                ie.putExtra("productUniqueId",vacineUniqueId);
//                startActivity(ie);

                Bundle b=new Bundle();
                b.putString("productId",ProductId );
                gotoNext(ProductDetailsFromAllProductActivity.class,b);

            }
        });


    }

    @Override
    public void successfullyGetAllProducts(AllProductResponse allProductResponse) {
        if(allProductResponse.getResponseCode()==1)
        {
            adapter.loadList(allProductResponse.getAllProduct());
        }
        else
        {

        }
    }
}
