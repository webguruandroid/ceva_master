package app.ceva.petapp.feature.Dashboard;

import app.ceva.petapp.data.network.model.NotificationCount.NotificationCountResponse;
import app.ceva.petapp.data.network.model.ScanQr.ScanQrResponse;
import app.ceva.petapp.data.network.model.Slug.SlugResponse;
import app.ceva.petapp.data.network.model.UpdateToken.UpdateTokenResponse;
import app.ceva.petapp.share.base.MvpView;

public interface DashboardMvpView extends MvpView {

    void navigateToLogout();
    void onSuccessfullScanQr(ScanQrResponse scanQrResponse);
    void ongetNotificationCount(NotificationCountResponse notificationCountResponse);
    void ongetUpdateToken(UpdateTokenResponse updateTokenResponse);
    void onGetSlugData(SlugResponse slugResponse);
}
