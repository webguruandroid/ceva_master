package app.ceva.petapp.feature.Notification;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import javax.inject.Inject;

import app.ceva.petapp.R;
import app.ceva.petapp.data.network.model.Notification.NotificationResponse;
import app.ceva.petapp.data.network.model.readnotification.ReadNotificationResponse;
import app.ceva.petapp.feature.ProductDetails.ProductDetailsActivity;
import app.ceva.petapp.share.base.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationActivity extends BaseActivity implements NotificationMvpView {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_allnatification)
    RecyclerView rv_allnatification;
    @BindView(R.id.llEmpty)
    RelativeLayout llEmpty;

    @Inject
    NotificationListAdapter adapter;

    NotificationResponse.ResponseDataBean bean;

    @Inject
    NotificationPresenter<NotificationMvpView> presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        setUp();
        setToolbar();
        setRecylerView();
    }

    protected void setUp()
    {
        presenter.onAttached(this);

    }

    private void setToolbar()
    {

        setSupportActionBar(toolbar);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onGetAllNotification();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void setRecylerView() {
        rv_allnatification.setLayoutManager(new LinearLayoutManager(this));
        rv_allnatification.setAdapter(adapter);


        adapter.setAdapterListner(new NotificationListAdapter.NotificationListListner() {
            @Override
            public void onItemClick(NotificationResponse.ResponseDataBean item, int position, String petId, String productId,String UniqueId) {




                bean=item;

                presenter.onReadNotification(item.getNotification_id());



            }
        });




    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void successfullyNotification(NotificationResponse notificationResponse) {



        if(notificationResponse.getResponseCode()==1)
        {
            if (notificationResponse.getResponseData().size() >0) {
                rv_allnatification.setVisibility(View.VISIBLE);
                llEmpty.setVisibility(View.GONE);
                adapter.loadList(notificationResponse.getResponseData());
            }
            else
            {
                rv_allnatification.setVisibility(View.GONE);
                llEmpty.setVisibility(View.VISIBLE);
            }

        }


    }

    @Override
    public void successfullyReadNotification(ReadNotificationResponse readNotificationResponse) {


        if(readNotificationResponse.getResponseCode()==1)
        {
            Intent ie = new Intent(NotificationActivity.this, ProductDetailsActivity.class);
            ie.putExtra("productId",bean.getProduct_id() );
            ie.putExtra("productUniqueId",bean.getProduct_uniqueId());
            ie.putExtra("tag", "close");
            ie.putExtra("pet_id", bean.getPet_id());
            startActivity(ie);
        }
    }
}
