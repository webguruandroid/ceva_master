package app.ceva.petapp.feature.ProductDetailsFromAllProduct;

import app.ceva.petapp.share.base.MvpPresenter;

public interface ProductDetailsFromAllProductsMvpPresenter <V extends ProductDetailsFromAllProductMvpView>extends MvpPresenter<V> {

    void onGetProductDetaild(String productId);
    void onGetAllPets(String userId);
    void onSubmitSchedule(String petId,String productId,String date);
}
