package app.ceva.petapp.feature.Notification;

import javax.inject.Inject;

import app.ceva.petapp.data.DataManager;
import app.ceva.petapp.data.network.model.Notification.NotificationRequest;
import app.ceva.petapp.data.network.model.Notification.NotificationResponse;
import app.ceva.petapp.data.network.model.readnotification.ReadNotificationRequest;
import app.ceva.petapp.data.network.model.readnotification.ReadNotificationResponse;
import app.ceva.petapp.share.base.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationPresenter <V extends NotificationMvpView> extends BasePresenter<V> implements NotificationMvpPresenter<V> {

    @Inject
    public NotificationPresenter(DataManager mdataManager) {
        super(mdataManager);
    }

    @Override
    public void onGetAllNotification() {


        if(getMvpView().isNetworkConnected())
        {
            getMvpView().showLoading();
            NotificationRequest notificationRequest=new NotificationRequest(getDataManager().getUserId(),"A");
            getDataManager().getNotification(notificationRequest).enqueue(new Callback<NotificationResponse>() {
                @Override
                public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {

                    if(response.isSuccessful())
                    {
                        getMvpView().hideLoading();
                        if(response.body().getResponseCode()==1)
                        {
                           // getMvpView().onError(response.body().getResponseText());
                            getMvpView().successfullyNotification(response.body());

                        }
                        else
                        {
                           // getMvpView().onError(response.body().getResponseText());
                            getMvpView().successfullyNotification(response.body());
                        }
                    }
                    else
                    {
                        getMvpView().hideLoading();
                        getMvpView().onError("Server Error");
                    }

                }

                @Override
                public void onFailure(Call<NotificationResponse> call, Throwable t) {
                    getMvpView().hideLoading();
                    getMvpView().showAlert("Server Error");
                }
            });

        }
        else
        {
            getMvpView().showAlert("No internet Connection");
        }
    }

    @Override
    public void onReadNotification(String notificationid) {

        if(getMvpView().isNetworkConnected())
        {

            ReadNotificationRequest notificationRequest=new ReadNotificationRequest(getDataManager().getUserId(),notificationid);
            getDataManager().getReadNotification(notificationRequest).enqueue(new Callback<ReadNotificationResponse>() {
                @Override
                public void onResponse(Call<ReadNotificationResponse> call, Response<ReadNotificationResponse> response) {

                    if(response.isSuccessful())
                    {

                        if(response.body().getResponseCode()==1)
                        {
                            // getMvpView().onError(response.body().getResponseText());
                            getMvpView().successfullyReadNotification(response.body());

                        }
                        else
                        {
                            // getMvpView().onError(response.body().getResponseText());
                            getMvpView().successfullyReadNotification(response.body());
                        }
                    }
                    else
                    {
                        getMvpView().hideLoading();
                        getMvpView().onError("Server Error");
                    }

                }

                @Override
                public void onFailure(Call<ReadNotificationResponse> call, Throwable t) {
                    getMvpView().hideLoading();
                    getMvpView().showAlert("Server Error");
                }
            });

        }
        else
        {
            getMvpView().showAlert("No internet Connection");
        }
    }
}
