package app.ceva.petapp.feature.AllPage;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ScrollView;

import androidx.appcompat.widget.Toolbar;

import javax.inject.Inject;

import app.ceva.petapp.R;
import app.ceva.petapp.data.network.model.Allpages.AllpagesResponse;
import app.ceva.petapp.share.base.BaseActivity;
import app.ceva.petapp.share.wigeds.TextViewBold;
import app.ceva.petapp.share.wigeds.TextViewRegular;
import app.ceva.petapp.utils.AppData;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AllPagesActivity extends BaseActivity implements AllPagesMvpView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @Inject
    AllpagePresenter<AllPagesMvpView> presenter;
    @BindView(R.id.tvtop)
    TextViewBold tvtop;
    @BindView(R.id.tvContent)
    TextViewRegular tvContent;
    @BindView(R.id.sv_content)
    ScrollView sv_content;
    @BindView(R.id.webView)
    WebView webView;
    private String tag;
    Typeface font;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        setUp();
        setToolbar();
    }
    protected void setUp()
    {
         font = Typeface.createFromAsset(getAssets(), "fonts/lato_medium.ttf");
        presenter.onAttached(this);

        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setFixedFontFamily("fonts/lato_medium.ttf");
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.getSettings().setDefaultFontSize((int) getResources().getDimension(R.dimen._17sdp));

    }

    @Override
    protected void onResume() {
        super.onResume();
        getIntentData();
    }

    private void setToolbar()
    {

        setSupportActionBar(toolbar);
        //  toolbar.setNavigationIcon(R.drawable.ic_left_arrow);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow);
        // upArrow.setColorFilter(getResources().getColor(R.color.grey), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
    }

    private void getIntentData()
    {
        tag=getIntent().getStringExtra("tag");
        presenter.ongetContent(tag);
        if(tag.equals(AppData.Contact_Us_Slug))
        {
            tvContent.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
            sv_content.setVisibility(View.GONE);
        }
        else if(tag.equals(AppData.About_Us_Slug))
        {
            tvContent.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
            sv_content.setVisibility(View.GONE);
        }
        else if(tag.equals(AppData.Privacy_polisy_Slug))
        {
            tvContent.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
            sv_content.setVisibility(View.GONE);
        }
        else if(tag.equals(AppData.Terms_Condition_Slug))
        {
            tvContent.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
            sv_content.setVisibility(View.GONE);
        }
    }

    @Override
    public void onsuccessfullyGetAllPeg(AllpagesResponse allpagesResponse) {


        if(allpagesResponse.getResponseCode()==1)
        {
            tvContent.setText(allpagesResponse.getResponseData().getPage_data());

            tvtop.setText(allpagesResponse.getResponseData().getPage_name());
            webView.loadDataWithBaseURL(null,allpagesResponse.getResponseData().getPage_data(),"text/html", "utf-8", null);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here


                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
