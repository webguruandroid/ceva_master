package app.ceva.petapp.data.network.model.NotificationCount;

public class NotificationCountResponse {


    /**
     * responseCode : 1
     * responseText : Notification count found.
     * responseData : 2
     */

    private int responseCode;
    private String responseText;
    private int responseData;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public int getResponseData() {
        return responseData;
    }

    public void setResponseData(int responseData) {
        this.responseData = responseData;
    }
}
