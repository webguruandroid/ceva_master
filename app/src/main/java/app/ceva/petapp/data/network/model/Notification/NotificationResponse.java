package app.ceva.petapp.data.network.model.Notification;

import java.util.List;

public class NotificationResponse {


    /**
     * responseCode : 1
     * responseText : Notification list found.
     * responseData : [{"notification_id":"4","pet_id":"3","pet_name":"Alex","product_id":"23","product_name":"test data2","message":"Your pet vaccine date is 2020-02-01","vaccine_date":"2020-02-01"}]
     */

    private int responseCode;
    private String responseText;
    private List<ResponseDataBean> responseData;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public List<ResponseDataBean> getResponseData() {
        return responseData;
    }

    public void setResponseData(List<ResponseDataBean> responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean {
        /**
         * notification_id : 4
         * pet_id : 3
         * pet_name : Alex
         * product_id : 23
         * product_name : test data2
         * message : Your pet vaccine date is 2020-02-01
         * vaccine_date : 2020-02-01
         */

        private String notification_id;
        private String pet_id;
        private String pet_name;
        private String product_id;
        private String product_name;
        private String message;
        private String vaccine_date;
        private String product_uniqueId;
        private String is_read;

        public String getIs_read() {
            return is_read;
        }

        public void setIs_read(String is_read) {
            this.is_read = is_read;
        }

        public String getProduct_uniqueId() {
            return product_uniqueId;
        }

        public void setProduct_uniqueId(String product_uniqueId) {
            this.product_uniqueId = product_uniqueId;
        }

        public String getNotification_id() {
            return notification_id;
        }

        public void setNotification_id(String notification_id) {
            this.notification_id = notification_id;
        }

        public String getPet_id() {
            return pet_id;
        }

        public void setPet_id(String pet_id) {
            this.pet_id = pet_id;
        }

        public String getPet_name() {
            return pet_name;
        }

        public void setPet_name(String pet_name) {
            this.pet_name = pet_name;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getVaccine_date() {
            return vaccine_date;
        }

        public void setVaccine_date(String vaccine_date) {
            this.vaccine_date = vaccine_date;
        }
    }
}
