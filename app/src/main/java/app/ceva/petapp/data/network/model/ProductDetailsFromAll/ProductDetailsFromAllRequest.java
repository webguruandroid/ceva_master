package app.ceva.petapp.data.network.model.ProductDetailsFromAll;

public class ProductDetailsFromAllRequest {


    /**
     * product_id : 22
     * user_id : 7
     */

    private String product_id;
    private String user_id;

    public ProductDetailsFromAllRequest(String product_id, String user_id) {
        this.product_id = product_id;
        this.user_id = user_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
