package app.ceva.petapp.data.network.model.Slug;

import java.util.List;

public class SlugResponse {

    /**
     * responseCode : 1
     * responseText : Slug List
     * responseData : [{"page_id":1,"page_title":"Contact Us","page_slug":"contact_us"},{"page_id":2,"page_title":"About Us","page_slug":"about_us"},{"page_id":3,"page_title":"Privacy Policy","page_slug":"privacy_policy"},{"page_id":4,"page_title":"Terms and Conditions","page_slug":"terms"}]
     */

    private int responseCode;
    private String responseText;
    private List<ResponseDataBean> responseData;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public List<ResponseDataBean> getResponseData() {
        return responseData;
    }

    public void setResponseData(List<ResponseDataBean> responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean {
        /**
         * page_id : 1
         * page_title : Contact Us
         * page_slug : contact_us
         */

        private int page_id;
        private String page_title;
        private String page_slug;

        public int getPage_id() {
            return page_id;
        }

        public void setPage_id(int page_id) {
            this.page_id = page_id;
        }

        public String getPage_title() {
            return page_title;
        }

        public void setPage_title(String page_title) {
            this.page_title = page_title;
        }

        public String getPage_slug() {
            return page_slug;
        }

        public void setPage_slug(String page_slug) {
            this.page_slug = page_slug;
        }
    }
}
