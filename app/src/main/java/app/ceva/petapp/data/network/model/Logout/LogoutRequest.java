package app.ceva.petapp.data.network.model.Logout;

public class LogoutRequest {

    /**
     * phone : +9717278768453
     * device_type : I
     */

    private String phone;
    private String device_type;


    public LogoutRequest(String phone, String device_type) {
        this.phone = phone;
        this.device_type = device_type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }
}
