package app.ceva.petapp.data.network.model.Notification;

public class NotificationRequest {


    /**
     * user_id : 7
     */

    private String user_id;
    private String device_type;

    public NotificationRequest(String user_id, String device_type) {
        this.user_id = user_id;
        this.device_type = device_type;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
