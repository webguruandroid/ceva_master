package app.ceva.petapp.data.network.model.DeleteVacine;

public class DeleteVacineRequest {

    private String user_id;
    private String row_id;

    public DeleteVacineRequest(String user_id, String row_id) {
        this.user_id = user_id;
        this.row_id = row_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRow_id() {
        return row_id;
    }

    public void setRow_id(String row_id) {
        this.row_id = row_id;
    }
}
