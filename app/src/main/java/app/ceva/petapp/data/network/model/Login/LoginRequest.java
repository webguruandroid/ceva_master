package app.ceva.petapp.data.network.model.Login;

public class LoginRequest {

    /**
     * phone : 1234567890
     * password : 123456
     */

    private String phone;
    private String password;
    private String device_type;
    private String device_token;

    public LoginRequest(String phone, String password, String device_type, String device_token) {
        this.phone = phone;
        this.password = password;
        this.device_type = device_type;
        this.device_token = device_token;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
