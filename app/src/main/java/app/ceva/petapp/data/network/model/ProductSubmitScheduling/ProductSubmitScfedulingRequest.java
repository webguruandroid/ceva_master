package app.ceva.petapp.data.network.model.ProductSubmitScheduling;

public class ProductSubmitScfedulingRequest {

    private String user_id;
    private String pet_id;
    private String product_id;
    private String date;

    public ProductSubmitScfedulingRequest(String user_id, String pet_id, String product_id, String date) {
        this.user_id = user_id;
        this.pet_id = pet_id;
        this.product_id = product_id;
        this.date = date;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPet_id() {
        return pet_id;
    }

    public void setPet_id(String pet_id) {
        this.pet_id = pet_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
