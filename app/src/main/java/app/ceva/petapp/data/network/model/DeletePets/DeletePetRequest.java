package app.ceva.petapp.data.network.model.DeletePets;

public class DeletePetRequest {


    /**
     * pet_id : 3
     */

    private String pet_id;
    private String user_id;
    private String message;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



    public String getPet_id() {
        return pet_id;
    }

    public void setPet_id(String pet_id) {
        this.pet_id = pet_id;
    }

    public DeletePetRequest(String pet_id, String user_id, String message) {
        this.pet_id = pet_id;
        this.user_id = user_id;
        this.message = message;
    }
}
