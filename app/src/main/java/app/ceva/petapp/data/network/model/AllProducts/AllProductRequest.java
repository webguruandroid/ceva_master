package app.ceva.petapp.data.network.model.AllProducts;

public class AllProductRequest {


    /**
     * user_id : 1
     */

    private String user_id;

    public AllProductRequest(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
