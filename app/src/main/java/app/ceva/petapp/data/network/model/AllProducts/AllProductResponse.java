package app.ceva.petapp.data.network.model.AllProducts;

import java.util.List;

public class AllProductResponse {


    /**
     * responseCode : 1
     * responseText : Successfully Product Found.
     * allProduct : [{"product_id":"22","product_name":"test data"},{"product_id":"17","product_name":"admin new product"},{"product_id":"14","product_name":"test data"}]
     */

    private int responseCode;
    private String responseText;
    private List<AllProductBean> allProduct;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public List<AllProductBean> getAllProduct() {
        return allProduct;
    }

    public void setAllProduct(List<AllProductBean> allProduct) {
        this.allProduct = allProduct;
    }

    public static class AllProductBean {
        /**
         * product_id : 22
         * product_name : test data
         */

        private String product_id;
        private String product_name;

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }
    }
}
