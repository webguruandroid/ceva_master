package app.ceva.petapp.data.network.model.UpdateToken;

public class UpdateTokenResponse {

    /**
     * responseCode : 1
     * responseText : Successfully Update Token
     */

    private int responseCode;
    private String responseText;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }
}
