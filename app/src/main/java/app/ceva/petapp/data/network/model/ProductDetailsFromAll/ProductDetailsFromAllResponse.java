package app.ceva.petapp.data.network.model.ProductDetailsFromAll;

import java.util.List;

public class ProductDetailsFromAllResponse {


    /**
     * responseCode : 1
     * responseText : Successfully Product Found.
     * allProduct : {"product_id":"22","product_name":"test data","product_info":"sdfsdf","product_indications":"sdfsdf","product_description":"sdfds","product_pdf":[{"image":"http://192.168.5.51/QRcode/storage/app/pdf/cY.JG.para(1).pdf"},{"image":"http://192.168.5.51/QRcode/storage/app/pdf/4Q.ad.ACP Injection (Aug2016).pdf"}],"product_image":"http://192.168.5.51/QRcode/storage/app/productImage/1573634910.png"}
     */

    private int responseCode;
    private String responseText;
    private AllProductBean allProduct;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public AllProductBean getAllProduct() {
        return allProduct;
    }

    public void setAllProduct(AllProductBean allProduct) {
        this.allProduct = allProduct;
    }

    public static class AllProductBean {
        /**
         * product_id : 22
         * product_name : test data
         * product_info : sdfsdf
         * product_indications : sdfsdf
         * product_description : sdfds
         * product_pdf : [{"image":"http://192.168.5.51/QRcode/storage/app/pdf/cY.JG.para(1).pdf"},{"image":"http://192.168.5.51/QRcode/storage/app/pdf/4Q.ad.ACP Injection (Aug2016).pdf"}]
         * product_image : http://192.168.5.51/QRcode/storage/app/productImage/1573634910.png
         */

        private String product_id;
        private String product_name;
        private String product_info;
        private String product_indications;
        private String product_description;
        private String product_image;
        private String is_schedule;
        private List<ProductPdfBean> product_pdf;


        public String getIs_schedule() {
            return is_schedule;
        }

        public void setIs_schedule(String is_schedule) {
            this.is_schedule = is_schedule;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getProduct_info() {
            return product_info;
        }

        public void setProduct_info(String product_info) {
            this.product_info = product_info;
        }

        public String getProduct_indications() {
            return product_indications;
        }

        public void setProduct_indications(String product_indications) {
            this.product_indications = product_indications;
        }

        public String getProduct_description() {
            return product_description;
        }

        public void setProduct_description(String product_description) {
            this.product_description = product_description;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public List<ProductPdfBean> getProduct_pdf() {
            return product_pdf;
        }

        public void setProduct_pdf(List<ProductPdfBean> product_pdf) {
            this.product_pdf = product_pdf;
        }

        public static class ProductPdfBean {
            /**
             * image : http://192.168.5.51/QRcode/storage/app/pdf/cY.JG.para(1).pdf
             */

            private String image;

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }
        }


    }

    @Override
    public String toString() {
        return "ProductDetailsFromAllResponse{" +
                "responseCode=" + responseCode +
                ", responseText='" + responseText + '\'' +
                ", allProduct=" + allProduct +
                '}';
    }
}
