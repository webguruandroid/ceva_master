package app.ceva.petapp.data.network.model.UpdateToken;

public class UpdateTokenRequest {

    /**
     * user_id : 5
     * device_type :
     * device_token :
     */

    private String user_id;
    private String device_type;
    private String device_token;

    public UpdateTokenRequest(String user_id, String device_type, String device_token) {
        this.user_id = user_id;
        this.device_type = device_type;
        this.device_token = device_token;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }
}
