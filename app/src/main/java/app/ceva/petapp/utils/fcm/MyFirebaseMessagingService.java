package app.ceva.petapp.utils.fcm;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import javax.inject.Inject;

import app.ceva.petapp.R;
import app.ceva.petapp.data.DataManager;
import app.ceva.petapp.feature.ProductDetails.ProductDetailsActivity;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Inject
    DataManager mDataManager;
    Bitmap imageBitmap;
    Uri defaultSoundUri;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        try {
            Log.e("msgreceived" ,"noti-"+remoteMessage.getData().get("pet_id")+"-"+remoteMessage.getData().get("product_id")+"-"+remoteMessage.getData().get("product_uniqueId"));
            Log.e("msgreceived" ,"body-"+remoteMessage.getNotification().getBody()+"-"+remoteMessage.getNotification().getTitle()+"-");
            if(remoteMessage.getData()!=null)
            {

                sendNotificationProduct(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody(),remoteMessage.getData().get("product_uniqueId"), remoteMessage.getData().get("pet_id"), remoteMessage.getData().get("product_id"));


            }


        } catch (Exception e) {
            e.printStackTrace();
        }



    }



    public void sendNotificationProduct(String title,String message,String uniqueid,String petId,String productid)
    {
        Intent intent=null;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
             Log.e("msgreceived","less O");
            Log.e("msgreceived",title+message+uniqueid+productid);
            Random random = new Random();
            int m = random.nextInt(9999 - 1000) + 1000;




            Intent ie = new Intent(this, ProductDetailsActivity.class);
            ie.putExtra("productId", productid);
            ie.putExtra("productUniqueId", uniqueid);
            ie.putExtra("tag", "close");
            ie.putExtra("pet_id", petId);



            Log.e("msgreceived",petId+message+uniqueid+productid);

            ie.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.logo);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, m, ie, PendingIntent.FLAG_ONE_SHOT);
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


            Log.e("msgreceived",petId+message+uniqueid+productid);
          //  imageBitmap=getBitmapfromUrl(image);


            NotificationCompat.Builder mBuilder1 = new NotificationCompat.Builder(this, "Default")
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setContentTitle(title)
                    .setLargeIcon(largeIcon)
                    .setAutoCancel(true)
                   // .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(imageBitmap).setSummaryText(message))
                    .setSound(sound)
                    .setContentText(message)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.mipmap.logo);

//

            manager.notify(m, mBuilder1.build());
            Log.e("msgreceived","end");

        }
        else
        {
             Log.e("msgreceived","greater O");
            Random random = new Random();
            int m = random.nextInt(9999 - 1000) + 1000;
            Intent ie = new Intent(this, ProductDetailsActivity.class);
            ie.putExtra("productId", productid);
            ie.putExtra("productUniqueId", uniqueid);
            ie.putExtra("tag", "close");
            ie.putExtra("pet_id", petId);

            Log.e("msgreceived",uniqueid+"="+productid);
          //  imageBitmap=getBitmapfromUrl(image);

          //  RemoteViews remoteViews=new RemoteViews(getPackageName(),R.layout.custome_notification);
            ie.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.logo);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, m, ie, PendingIntent.FLAG_ONE_SHOT);
            String channelId = "Default";
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Log.e("msgreceived",title+message+uniqueid+productid);

            createChannel(manager);
            NotificationCompat.Builder mBuilder1 = new NotificationCompat.Builder(this, "Default")
                    .setChannelId(channelId)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setContentTitle(title)
                    .setLargeIcon(largeIcon)
                    .setAutoCancel(true)
                    .setSound(sound)
                    .setSmallIcon(R.mipmap.logo)
                   // .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(imageBitmap).setSummaryText(message))
                    .setContentText(message)
                    .setContentIntent(pendingIntent);

            manager.notify(m, mBuilder1.build());

            Log.e("msgreceived","end");
        }


    }





    @TargetApi(26)
    private void createChannel(NotificationManager notificationManager) {

        int importance = NotificationManager.IMPORTANCE_DEFAULT;

        NotificationChannel mChannel = new NotificationChannel("Default", "Default channel", importance);

        mChannel.enableLights(true);
        mChannel.setLightColor(Color.BLUE);
        mChannel.setShowBadge(true);
        notificationManager.createNotificationChannel(mChannel);
    }


    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }






}
